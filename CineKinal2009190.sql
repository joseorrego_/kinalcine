CREATE DATABASE CineKinal2009190;

USE CineKinal2009190;

CREATE TABLE Pelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(100) NOT NULL,
    sinopsis VARCHAR(500) NOT NULL,
    trailer_url VARCHAR(200) NOT NULL,
    image VARCHAR (250) NOT NULL,
    rated VARCHAR(20) NOT NULL,
    genero VARCHAR(50) NOT NULL
);

CREATE TABLE FormatoPelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Cine(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    direccion VARCHAR(500) NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    latitud DOUBLE(20, 18) NOT NULL,
    longitud DOUBLE(20, 18) NOT NULL,
    hora_apertura TIME NOT NULL,
    hora_cierre TIME NOT NULL
);

CREATE TABLE TipoSala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Sala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cine_id INT NOT NULL,
    numero INT NOT NULL,
    tiposala_id INT NOT NULL,
    FOREIGN KEY (cine_id) REFERENCES Cine(id),
    FOREIGN KEY (tiposala_id) REFERENCES TipoSala(id)
);

CREATE TABLE Cartelera(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sala_id INT NOT NULL,
    pelicula_id INT NOT NULL,
    formatopelicula_id INT NOT NULL,
    formato_lenguaje VARCHAR(10) NOT NULL,
    fecha DATE NOT NULL,
    hora TIME NOT NULL,
    FOREIGN KEY (sala_id) REFERENCES Sala(id),
    FOREIGN KEY (pelicula_id) REFERENCES Pelicula(id),
    FOREIGN KEY (formatopelicula_id) REFERENCES FormatoPelicula(id)
);



---------------------------------------------------------------------------------------
//Pelicula
insert into Pelicula(titulo,sinopsis,trailer_url,image,rated,genero) values('Los Juegos del Hambre - Sinsajo parte 1','Después de ser rescatado de la arena destruida en los 75º Juegos del Hambre, Katniss Everdeen, junto con sus compañeros Vencedores Beetee Latier y Finnick Odair, son llevados al Distrito 13, un distrito independiente aislado del resto de Panem que ha sido la punta de lanza de la rebelión, donde se reúne con su madre y su hermana Prim.','https://www.youtube.com/embed/V6qe8BiQYOI','data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIALoAewMBIgACEQE','A','Accion');
insert into Pelicula(titulo,sinopsis,trailer_url,image,rated,genero) values('Grandes Heroes',' es una comedia de aventuras plagada de acción protagonizada por Hiro Hamada, un prodigio de la robótica que aprende a aprovechar todo su genio gracias a su hermano Tadashi y a sus amigos de similares intereses: la adicta a la adrenalina Go Go Tomago, el fanático de la limpieza Wasabi No-Ginger, la genio de la química Honey Lemon y el divertido Fred.','https://www.youtube.com/embed/DrAClyAk4O8','data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAFoAoQMBIgACEQE','C','Comedia');
insert into Pelicula(titulo,sinopsis,trailer_url,image,rated,genero) values('Intensamente','A medida que Riley crece, se van agregando emociones, siendo Alegría la primera y Furia/Ira la última emoción en unirse. Las emociones viven en el cuartel general, la mente consciente de Riley, desde el que influyen en las acciones y los recuerdos de Riley a través de una consola de control.','https://www.youtube.com/embed/VPLpLLHROTs','https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTXffqgyqDcp4mh2zp4mCVtPwCYBF8-Y338auK16TrJDtid0b3LcTk5fnQQ','A','Accion');
SELECT * FROM Pelicula;

---------------------------------------------------------------------------------------
//FormatoPelicula

insert into FormatoPelicula(nombre,descripcion) values('A','Para todo publico');
SELECT * FROM FormatoPelicula;

---------------------------------------------------------------------------------------
//Cine

insert into Cine(nombre,direccion,telefono,latitud,longitud,hora_apertura,hora_cierre) values('Cine Eskala','Guatemala, Guatemala',25487985,15.000000000000000000,15.000000000000000000,00:00:07,00:00:08);
insert into Cine(nombre,direccion,telefono,latitud,longitud,hora_apertura,hora_cierre) values('Cine Miraflores','Guatemala',32568978,100.000000000000000000,100.000000000000000000,00:00:06,00:00:23);
SELECT * FROM Cine;

---------------------------------------------------------------------------------------
//TipoSala

insert into TipoSala(nombre,descripcion) values('2D','Es una sola normal sin uso de lentes');
SELECT * FROM TipoSala;

---------------------------------------------------------------------------------------
//Sala

insert into Sala(cine_id,numero,tiposala_id) values(2,14,1);
SELECT * FROM Sala;

----------------------------------------------------------------------------------------
//Cartelera

insert into Cartelera (sala_id,pelicula_id,formatopelicula_id,formato_lenguaje,fecha,hora) values(4,2,1,'Español',2015-12-23,18:00:00);
SELECT * FROM Cartelera;

