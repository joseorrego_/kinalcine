<?php
/**
 * Created by PhpStorm.
 * User: familia
 * Date: 2/08/15
 * Time: 07:47 PM
 */

class VistaTipo extends BaseController
{
    public function getIndex()
    {
        $tipof = TipoSala::get();
        return View::make('Tipo.index')->with('tipo', $tipof);
    }

    public function getCreate()
    {
        return View::make('Sala.create');
    }

    public function store()
    {
        $tipof = new Salas;

        $tipof->nombre = Input::get('nombre');
        $tipof->descripcion = Input::get('descripcion');


        if ($tipof->save()) {
            Session::flash('message','Guardado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine/create');
    }

    public function getShow($id)
    {
        $tipof = TipoSala::find($id);

        return View::make('Tipo.show')->with('tipo',$tipof);
    }

    public function getEdit($id)
    {
        $tipof = TipoSala::find($id);

        return View::make('Tipo.edit')->with('tipo',$tipof);
    }

    public function update($id)
    {
        $tipof = TipoSala::find($id);

        $tipof->nombre = Input::get('nombre');
        $tipof->descripcion = Input::get('descripcion');

        if ($tipof->save()) {
            Session::flash('message','Actualizado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Tipo/edit/'.$id);
    }

    public function destroy($id)
    {
        $tipof = TipoSala::find($id);

        if ($tipof->delete()) {
            Session::flash('message','Eliminado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Tipo');
    }
}