<?php
/**
 * Created by PhpStorm.
 * User: familia
 * Date: 1/08/15
 * Time: 09:02 PM
 *

 */
class VistaPeliculas extends BaseController{
    public function getIndex(){
     $pelciculas = Peliculas::get();
        return View::make('Pelicula.index')->with('pelicula',$pelciculas);
    }

    public function getCreate()
    {
        return View::make('Pelicula.create');
    }

    public function store()
    {
        $pelicula = new Peliculas;

        $pelicula->titulo = Input::get('titulo');
        $pelicula->sinopsis = Input::get('sinopsis');

        if ($pelicula->save()) {
            Session::flash('message','Guardado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Pelicula/create');
    }


    public function getShow($id)
    {
        $pelicula = Peliculas::find($id);

        return View::make('Pelicula.show')->with('pelicula',$pelicula);
    }

    public function getEdit($id)
    {
        $pelicula = Peliculas::find($id);

        return View::make('Pelicula.edit')->with('pelicula',$pelicula);
    }

    public function update($id)
    {
        $pelicula = Peliculas::find($id);

        $pelicula->titulo = Input::get('titulo');
        $pelicula->sinopsis = Input::get('sinopsis');

        if ($pelicula->save()) {
            Session::flash('message','Actualizado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Pelicula/edit/'.$id);
    }

    public function destroy($id)
    {
        $pelicula = Peliculas::find($id);

        if ($pelicula->delete()) {
            Session::flash('message','Eliminado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Pelicula');
    }

}