<?php
/**
 * Created by PhpStorm.
 * User: familia
 * Date: 2/08/15
 * Time: 06:04 PM
 */

class VistaSala extends BaseController{
    public function getIndex()
    {
        $room = Salas::get();
        return View::make('Sala.index')->with('sala', $room);
    }

    public function getCreate()
    {
        return View::make('Sala.create');
    }

    public function store()
    {
        $room = new Salas;

        $room->cine_id = Input::get('cine_id');
        $room->numero = Input::get('numero');
        $room->tiposala_id = Input::get('tiposala_id');

        if ($room->save()) {
            Session::flash('message','Guardado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine/create');
    }

    public function getShow($id)
    {
        $room = Salas::find($id);

        return View::make('Sala.show')->with('sala',$room);
    }


    public function getEdit($id)
    {
        $room = Peliculas::find($id);

        return View::make('Sala.edit')->with('sala',$room);
    }

    public function update($id)
    {
        $room = Peliculas::find($id);

        $room->cine_id = Input::get('cine_id');
        $room->numero = Input::get('numero');
        $room->tiposala_id = Input::get('tiposala_id');

        if ($room->save()) {
            Session::flash('message','Actualizado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Sala/edit/'.$id);
    }

    public function destroy($id)
    {
        $room = Cines::find($id);

        if ($room->delete()) {
            Session::flash('message','Eliminado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine');
    }
}