<?php
/**
 * Created by PhpStorm.
 * User: familia
 * Date: 4/08/15
 * Time: 12:22 PM
 */
class VistaCartelera extends BaseController
{
    public function getIndex()
    {
        $cartelera = Cartelera::get();
        return View::make('Cartelera.index')->with('cartelera', $cartelera);
    }


    public function getCreate()
    {
        return View::make('Cartelera.create');
    }

    public function store()
    {
        $cartelera = new Cines;

        $cartelera->sala_id = Input::get('sala_id');
        $cartelera->pelicula_id = Input::get('pelicula_id');
        $cartelera->formatopelicula_id = Input::get('formatopelicula_id');
        $cartelera->formato_lenguaje = Input::get('formato_lenguaje');
        $cartelera->fecha = Input::get('fecha');
        $cartelera->hora = Input::get('hora');



        if ($cartelera->save()) {
            Session::flash('message','Guardado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cartelera/create');
    }

    public function getShow($id)
    {
        $cartelera = Cartelera::find($id);

        return View::make('Cartelera.show')->with('cartelera',$cartelera);
    }

    public function getEdit($id)
    {
        $cartelera = Cartelera::find($id);

        return View::make('Cartelera.edit')->with('cartelera',$cartelera);
    }

    public function update($id)
    {
        $cartelera = Cartelera::find($id);

        $cartelera->formato_lenguaje = Input::get('nombre');
        $cartelera->fecha = Input::get('direccion');
        $cartelera->hora = Input::get('telefono');

        if ($cartelera->save()) {
            Session::flash('message','Actualizado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cartelera/edit/'.$id);
    }

    public function destroy($id)
    {
        $cartelera = Cartelera::find($id);

        if ($cartelera->delete()) {
            Session::flash('message','Eliminado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cartelera');
    }

}