<?php
/**
 * Created by PhpStorm.
 * User: familia
 * Date: 1/08/15
 * Time: 09:42 PM
 */

class VistaCine extends BaseController{
    public function getIndex(){
        $movie = Cines::get();
            return View::make('Cine.index')->with('cine',$movie);
    }


    public function getCreate()
    {
        return View::make('Cine.create');
    }

    public function store()
    {
        $movie = new Cines;

        $movie->nombre = Input::get('nombre');
        $movie->direccion = Input::get('direccion');
        $movie->telefono = Input::get('telefono');
        $movie->latitud = Input::get('latitud');
        $movie->longitud = Input::get('longitud');
        $movie->hora_apertura = Input::get('hora_apertura');
        $movie->hora_cierre = Input::get('hora_cierre');


        if ($movie->save()) {
            Session::flash('message','Guardado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine/create');
    }

    public function getShow($id)
    {
        $movie = Cines::find($id);

        return View::make('Cine.show')->with('cine',$movie);
    }

    public function getEdit($id)
    {
        $movie = Cines::find($id);

        return View::make('Cine.edit')->with('cine',$movie);
    }

    public function update($id)
    {
        $movie = Cines::find($id);

        $movie->nombre = Input::get('nombre');
        $movie->direccion = Input::get('direccion');
        $movie->telefono = Input::get('telefono');

        if ($movie->save()) {
            Session::flash('message','Actualizado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine/edit/'.$id);
    }

    public function destroy($id)
    {
        $movie = Cines::find($id);

        if ($movie->delete()) {
            Session::flash('message','Eliminado correctamente!');
            Session::flash('class','success');
        } else {
            Session::flash('message','Ha ocurrido un error!');
            Session::flash('class','danger');
        }

        return Redirect::to('Cine');
    }
}
