<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Ver Tipo de Salas</title>

    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }



        .textoc{
            color: white;
            font-size: 45px;
            margin-left: 250px;
            color: white;
        }
        .textop{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
    </style>
</head>
<body>
<h1>Ver Tipo de Salas</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información de los tipos de salas</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($tipo))
            <p>
                Nombre: <strong>{{ $tipo->nombre }}</strong>
            </p>
            <p>
                Descripcion: <strong>{{ $tipo->descripcion }}</strong>
            </p>

        @else
            <p>
                No existe información para éste tipo de Tipo de Sala.
            </p>
        @endif

        <a href="/CineKinal2009190/public/Tipof" class="btn btn-default">Regresar</a>
    </div>
</div>
</body>