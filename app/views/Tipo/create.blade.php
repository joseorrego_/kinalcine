<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Nueva Sala</title>


    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Nuevo Tipo de Sala</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Nuevo Tipo de Sala</h4>
    </div>

    <div class="jumbotron">
        <form method="post" action="store">
            <p>
                Nombre:<input type="text" name="nombre" placeholder="Cine" class="form-control" required>
            </p>
            <p>
                Descripcion:<input type="text" name="descripcion" placeholder="numero" class="form-control" required>
            </p>
            <p>
                <input type="submit" value="Guardar" class="btn btn-success">
            </p>

            <a href="/CineKinal2009190/public/Tipof" class="btn btn-default">Regresar</a>
        </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>