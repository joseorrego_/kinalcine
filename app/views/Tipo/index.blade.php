<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Tipo Sala</title>
    <style>


        .banner{
            background: #20B2AA;
            width: 1400px;
            height: 60px;
        }

        .textoc{
            color: white;
            font-size: 45px;
            margin-left: 170px;

            color: white;
        }
        .textop{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .textot{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .inicio{
            color: white;
            font-size: 45px;
            margin-left: 15px;
            color: white;
        }
        h4{
            margin-bottom: 5px;
            color: white;
        }

        .navbar-nav{
            margin-right: 1360px;
        }
        .texton{
            color: black;
            margin-top: 30px;
        }

        .tit{
            color: black;
            text-align: center;
            font-size: 45px;
        }
    </style>
</head>
<body>
<div class="banner">
    <a class="inicio" href="/CineKinal2009190/public/" class="btn btn-default">Inicio</a>
    <a class="textoc" href="/CineKinal2009190/public/Pelicula" class="btn btn-default">Pelicula</a>
    <a class="textop" href="/CineKinal2009190/public/Sala" class="btn btn-default">Salas</a>
    <a class="textot" href="/CineKinal2009190/public/Cine" class="btn btn-default">Cine</a>
    <a class="textot" href="/CineKinal2009190/public/Cartelera" class="btn btn-default">Cartelera</a>

</div><div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="tit">Lista de Tipo Sala</h4>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <a class="texton" href="/CineKinal2009190/public//Tipof/create">Nueva Tipo de Sala</a>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Descripcion</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tipo as $tip)
                <tr>
                    <td>{{ $tip->id }}</td>
                    <td>{{ $tip->nombre }}</td>
                    <td>{{ $tip->descripcion }}</td>

                    <td>
                        <a href="Tipof/show/{{ $tip->id }}"><span class="label label-info">Ver</span></a>
                        <a href="Tipof/edit/{{ $tip->id }}"><span class="label label-success">Editar</span></a>
                        <a href="{{ url('Tipof/destroy',$tip->id) }}"><span class="label label-danger">Eliminar</span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>