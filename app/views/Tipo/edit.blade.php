<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Editar Tipo de Sala</title>
    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Editar Tipo de Sala</h1>

<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Editar Tipo de Sala</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($tipo))
            <form method="post" action="/CineKinal2009190/public/Tipo/update/{{ $tipo->id }}">
                <p>
                    Nombre:<input value="{{ $tipo->nombre }}" type="text" name="cine_id" placeholder="Cine" class="form-control" required>
                </p>
                <p>
                    Descripcion:<input value="{{ $tipo->descripcion }}" type="text" name="numero" placeholder="Numero" class="form-control" required>
                </p>
                <input type="submit" value="Guardar" class="btn btn-success">
                @else
                    <p>
                        No existe información para está Tipo de sala.
                    </p>
                @endif
                <a href="/CineKinal2009190/public/Tipof" class="btn btn-default">Regresar</a>
            </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>