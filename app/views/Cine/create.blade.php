<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Nuevo Cine</title>


    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Nuevo Cine</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Nuevo Cine</h4>
    </div>

    <div class="jumbotron">
        <form method="post" action="store">
            <p>
               Nombre:<input type="text" name="nombre" placeholder="nombre" class="form-control" required>
            </p>
            <p>
               Direccion: <input type="text" name="direccion" placeholder="direccion" class="form-control" required>
            </p>
            <p>
                Telefono:<input type="text" name="telefono" placeholder="telefono" class="form-control" required>
            </p>
            <p>
                Latitud:<input type="text" name="latitud" placeholder="latitud" class="form-control" required>
            </p>
            <p>
                Longuitud:<input type="text" name="longitud" placeholder="longitud" class="form-control" required>
            </p>
            <p>
                Hora Apertura:<input type="text" name="hora_apertura" placeholder="hora apertura" class="form-control" required>
            </p>
            <p>
                Hora Cierre:<input type="text" name="hora_cierre" placeholder="hora cierre" class="form-control" required>
            </p>

            <p>
                <input type="submit" value="Guardar" class="btn btn-success">
            </p>

            <a href="/CineKinal2009190/public/Cine" class="btn btn-default">Regresar</a>
        </form>
    </div>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>