<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Editar Cines</title>
    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Editar Cines</h1>

<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Editar Cines</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($cine))
            <form method="post" action="/CineKinal2009190/public/Cine/update/{{ $cine->id }}">
                <p>
                   Nombre: <input value="{{ $cine->nombre }}" type="text" name="nombre" placeholder="Nombre" class="form-control" required>
                </p>
                <p>
                  Direccion:  <input value="{{ $cine->direccion }}" type="text" name="direccion" placeholder="Direccion" class="form-control" required>
                </p>
                <p>
                    Telefono: <input value="{{ $cine->telefono }}" type="text" name="telefono" placeholder="Telefono" class="form-control" required>
                </p>
                <input type="submit" value="Guardar" class="btn btn-success">
                @else
                    <p>
                        No existe información para está Cine.
                    </p>
                @endif
                <a href="/CineKinal2009190/public/Cine" class="btn btn-default">Regresar</a>
            </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>