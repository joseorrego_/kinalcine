<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Ver Cines</title>

    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Ver Cines</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información del Cine</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($cine))
            <p>
                Nombre: <strong>{{ $cine->nombre }}</strong>
            </p>
            <p>
                Direccion: <strong>{{ $cine->direccion }}</strong>
            </p>
            <p>
                Telefono: <strong>{{ $cine->telefono }}</strong>
            </p>
        @else
            <p>
                No existe información para éste usuario.
            </p>
        @endif

        <a href="/CineKinal2009190/public/Cine" class="btn btn-default">Regresar</a>
    </div>
</div>
</body>
</html>