<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Nueva Pelicula</title>


    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Nueva Pelicula</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Nueva Pelicula</h4>
    </div>

    <div class="jumbotron">
        <form method="post" action="store">
            <p>
                Titulo:<input type="text" name="titulo" placeholder="titulo" class="form-control" required>
            </p>
            <p>
                Sinopsis:<input type="text" name="sinopsis" placeholder="Sinopsis" class="form-control" required>
            </p>
            <p>
                Trailer:<input type="text" name="trailer_url" placeholder="trailer" class="form-control" required>
            </p>
            <p>
                Imagen:<input type="text" name="image" placeholder="imagen" class="form-control" required>
            </p>
            <p>
                Clasificacion:<input type="text" name="rated" placeholder="Clasificacion" class="form-control" required>
            </p>
            <p>
                Genero:<input type="text" name="genero" placeholder="genero" class="form-control" required>
            </p>
            <p>
                <input type="submit" value="Guardar" class="btn btn-success">
            </p>

            <a href="/CineKinal2009190/public/Pelicula" class="btn btn-default">Regresar</a>
        </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>