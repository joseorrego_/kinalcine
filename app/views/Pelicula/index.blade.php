<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Peliculas</title>
    <style>


        .banner{
            background: #20B2AA;
            width: 1400px;
            height: 60px;
        }

        .textoc{
            color: white;
            font-size: 45px;
            margin-left: 170px;

            color: white;
        }
        .textop{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .textot{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .inicio{
            color: white;
            font-size: 45px;
            margin-left: 15px;
            color: white;
        }
        h4{
            margin-bottom: 5px;
            color: white;
        }

        .navbar-nav{
            margin-right: 1360px;
        }
        .texton{
            color: black;
            margin-top: 30px;
        }

        .tit{
            color: black;
            text-align: center;
            font-size: 45px;
        }
</style>
</head>
<body>
<div class="banner">
    <a class="inicio" href="/CineKinal2009190/public/" class="btn btn-default">Inicio</a>
    <a class="textoc" href="/CineKinal2009190/public/Cine" class="btn btn-default">Cine</a>
    <a class="textop" href="/CineKinal2009190/public/Sala" class="btn btn-default">Salas</a>
    <a class="textot" href="/CineKinal2009190/public/Tipof" class="btn btn-default">Tipo Formato</a>
    <a class="textot" href="/CineKinal2009190/public/Cartelera" class="btn btn-default">Cartelera</a>

</div>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="tit">Lista de peliculas</h4>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <a class="texton" href="/CineKinal2009190/public//Pelicula/create">Nueva Pelicula</a>
                    </ul>
                </div>
            </div>
        </nav>

    </div>

    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Sinopsis</th>
                <th>Video</th>
                <th>Imagen</th>
                <th>Calificacion</th>
                <th>Genero</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pelicula as $peli)
                <tr>
                    <td>{{ $peli->id }}</td>
                    <td>{{ $peli->titulo }}</td>
                    <td>{{ $peli->sinopsis }}</td>
                    <td><iframe width="460" height="215" src="{{ $peli->trailer_url }}" frameborder="0" allowfullscreen></iframe></td>
                    <td><img src="{{ $peli->image }}" alt="{{ $peli->titulo }}"></td>
                    <td>{{ $peli->rated }}</td>
                    <td>{{ $peli->genero }}</td>
                    <td>
                        <a href="Pelicula/show/{{ $peli->id }}"><span class="label label-info">Ver</span></a>
                        <a href="Pelicula/edit/{{ $peli->id }}"><span class="label label-success">Editar</span></a>
                        <a href="{{ url('Pelicula/destroy',$peli->id) }}"><span class="label label-danger">Eliminar</span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>