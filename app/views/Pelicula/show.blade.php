<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Ver Pelicula</title>

    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Ver Pelicula</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información de la pelicula</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($pelicula))
            <p>
                Titulo: <strong>{{ $pelicula->titulo }}</strong>
            </p>
            <p>
                Sinopsis: <strong>{{ $pelicula->sinopsis }}</strong>
            </p>
        @else
            <p>
                No existe información para éste usuario.
            </p>
        @endif

        <a href="/CineKinal2009190/public/Pelicula" class="btn btn-default">Regresar</a>
    </div>
</div>
</body>
</html>