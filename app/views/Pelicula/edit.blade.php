<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Editar Pelicula</title>
    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Editar Pelicula</h1>

<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Editar Pelicula</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($pelicula))
            <form method="post" action="/CineKinal2009190/public/Pelicula/update/{{ $pelicula->id }}">
                <p>
                   Titulo: <input value="{{ $pelicula->titulo }}" type="text" name="titulo" placeholder="Titulo" class="form-control" required>
                </p>
                <p>
                   Sinopsis: <input value="{{ $pelicula->sinopsis }}" type="text" name="sinopsis" placeholder="Sinopsis" class="form-control" required>
                </p>
                <input type="submit" value="Guardar" class="btn btn-success">
                @else
                    <p>
                        No existe información para está pelicula.
                    </p>
                @endif
                <a href="/CineKinal2009190/public/Pelicula" class="btn btn-default">Regresar</a>
            </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>