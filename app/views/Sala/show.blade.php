<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Ver Salas</title>

    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }



        .textoc{
            color: white;
            font-size: 45px;
            margin-left: 250px;
            color: white;
        }
        .textop{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
    </style>
</head>
<body>
<h1>Ver Salas</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información de las Salas</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($sala))
            <p>
                Cine: <strong>{{ $sala->cine_id }}</strong>
            </p>
            <p>
                Numero: <strong>{{ $sala->numero }}</strong>
            </p>
            <p>
                Tipo Sala: <strong>{{ $sala->tiposala_id }}</strong>
            </p>
        @else
            <p>
                No existe información para éste usuario.
            </p>
        @endif

        <a href="/CineKinal2009190/public/Sala" class="btn btn-default">Regresar</a>

    </div>
</div>
</body>
</html>