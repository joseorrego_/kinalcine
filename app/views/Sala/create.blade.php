<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Nueva Sala</title>


    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Nueva Sala</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Nueva Sala</h4>
    </div>

    <div class="jumbotron">
        <form method="post" action="store">
            <p>
                Cine id:<input type="text" name="cine_id" placeholder="Cine" class="form-control" required>
            </p>
            <p>
                Numero:<input type="text" name="numero" placeholder="numero" class="form-control" required>
            </p>
            <p>
                Tipo Sala id:<input type="text" name="tiposala_id" placeholder="tipo sala" class="form-control" required>
            </p>
            <p>
                <input type="submit" value="Guardar" class="btn btn-success">
            </p>

            <a href="/CineKinal2009190/public/Sala" class="btn btn-default">Regresar</a>
        </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>