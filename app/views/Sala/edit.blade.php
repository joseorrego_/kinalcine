<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Editar Sala</title>
    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Editar Sala</h1>

<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Editar Sala</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($sala))
            <form method="post" action="/CineKinal2009190/public/Sala/update/{{ $sala->id }}">
                <p>
                    <input value="{{ $sala->cine_id }}" type="text" name="cine_id" placeholder="Cine" class="form-control" required>
                </p>
                <p>
                    <input value="{{ $sala->numero }}" type="text" name="numero" placeholder="Numero" class="form-control" required>
                </p>
                <p>
                    <input value="{{ $sala->tiposala_id }}" type="text" name="tiposala_id" placeholder="Tipo Sala" class="form-control" required>
                </p>
                <input type="submit" value="Guardar" class="btn btn-success">
                @else
                    <p>
                        No existe información para está sala.
                    </p>
                @endif
                <a href="/CineKinal2009190/public/Sala" class="btn btn-default">Regresar</a>
            </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>