<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <title>Cartelera</title>
    <style>

        .banner{
            background: #20B2AA;
            width: 1400px;
            height: 60px;
        }

        .textoc{
            color: white;
            font-size: 45px;
            margin-left: 170px;

            color: white;
        }
        .textop{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .textot{
            color: white;
            font-size: 45px;
            margin-left: 20px;
            color: white;
        }
        .inicio{
            color: white;
            font-size: 45px;
            margin-left: 15px;
            color: white;
        }
        h4{
            margin-bottom: 5px;
            color: white;
        }

        .navbar-nav{
            margin-right: 1360px;
        }
        .texton{
            color: black;
            margin-top: 30px;
        }

        .tit{
            color: black;
            text-align: center;
            font-size: 45px;
        }
    </style>
</head>
<body>
<div class="banner">
    <a class="inicio" href="/CineKinal2009190/public/" class="btn btn-default">Inicio</a>
    <a class="textoc" href="/CineKinal2009190/public/Pelicula" class="btn btn-default">Pelicula</a>
    <a class="textot" href="/CineKinal2009190/public/Cine" class="btn btn-default">Cine</a>
    <a class="textop" href="/CineKinal2009190/public/Sala" class="btn btn-default">Salas</a>
    <a class="textot" href="/CineKinal2009190/public/Tipof" class="btn btn-default">Tipo Formato</a>

</div>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4 class="tit">Lista de Cartelera</h4>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <a class="texton" href="/CineKinal2009190/public//Cartelera/create">Nueva Cartelera</a>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="panel-body">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Sala id</th>
                <th>Pelicula id</th>
                <th>Formato de la pelicula</th>
                <th>Lenguaje</th>
                <th>Fecha</th>
                <th>Fecha</th>
                <th>Hora</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cartelera as $cart)
                <tr>
                    <td>{{ $cart->id }}</td>
                    <td>{{ $cart->sala_id }}</td>
                    <td>{{ $cart->pelicula_id }}</td>
                    <td>{{ $cart->formatopelicula_id }}</td>
                    <td>{{ $cart->formato_lenguaje }}</td>
                    <td>{{ $cart->fecha }}</td>
                    <td>{{ $cart->hora }}</td>
                    <td>
                    <td>
                        <a href="Cartelera/show/{{ $cart->id }}"><span class="label label-info">Ver</span></a>
                        <a href="Cartelera/edit/{{ $cart->id }}"><span class="label label-success">Editar</span></a>
                        <a href="{{ url('Cartelera/destroy',$cart->id) }}"><span class="label label-danger">Eliminar</span></a>
                    </td>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>