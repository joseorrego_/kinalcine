<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Ver Cartelera</title>

    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Ver Cartelera</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Información de la Cartelera</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($cartelera))
            <p>
                Lenguaje: <strong>{{ $cartelera->formato_lenguaje }}</strong>
            </p>
            <p>
                Fecha: <strong>{{ $cartelera->fecha }}</strong>
            </p>
            <p>
                Hora: <strong>{{ $cartelera->hora }}</strong>
            </p>
        @else
            <p>
                No existe información para está cartelera.
            </p>
        @endif

        <a href="/CineKinal2009190/public/Cartelera" class="btn btn-default">Regresar</a>
    </div>
</div>
</body>
</html>