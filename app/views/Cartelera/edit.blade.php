<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Editar Cartelera</title>
    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Editar Cartelera</h1>

<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Editar Cartelera</h4>
    </div>

    <div class="jumbotron">
        @if (!empty($cartelera))
            <form method="post" action="/CineKinal2009190/public/Cartelera/update/{{ $cartelera->id }}">
                <p>
                    Lenguaje: <input value="{{ $cartelera->formato_lenguaje }}" type="text" name="lenguaje" placeholder="Lenguaje" class="form-control" required>
                </p>
                <p>
                    Fecha:  <input value="{{ $cartelera->fecha }}" type="text" name="fecha" placeholder="Fecha" class="form-control" required>
                </p>
                <p>
                    Hora: <input value="{{ $cartelera->hora }}" type="text" name="hora" placeholder="Hora" class="form-control" required>
                </p>
                <input type="submit" value="Guardar" class="btn btn-success">
                @else
                    <p>
                        No existe información para está Cartelera.
                    </p>
                @endif
                <a href="/CineKinal2009190/public/Cartelera" class="btn btn-default">Regresar</a>
            </form>
    </div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>