<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <title>Nueva Cartelera</title>


    <style>
        body {
            width: 450px;
            margin: 50px auto;
        }
        .badge {
            float: right;
        }
    </style>
</head>
<body>
<h1>Nueva Cartelera</h1>
<div class="panel panel-success">
    <div class="panel-heading">
        <h4>Nueva Cartelera</h4>
    </div>

    <div class="jumbotron">
        <form method="post" action="store">

            <p>
                sala id: <input type="text" name="sala_id" placeholder="sala id" class="form-control" required>
            </p>
            <p>
                pelicula id:<input type="text" name="pelicula_id" placeholder="pelicula id" class="form-control" required>
            </p>
            <p>
                Formato Pelicula id:<input type="text" name="formatopelicula_id" placeholder="formato pelicula id" class="form-control" required>
            </p>
            <p>
                Lenguaje:<input type="text" name="formato_lenguaje" placeholder="lenguaje" class="form-control" required>
            </p>
            <p>
                Fecha:<input type="text" name="fecha" placeholder="fecha" class="form-control" required>
            </p>
            <p>
                Hora:<input type="text" name="hora" placeholder="hora" class="form-control" required>
            </p>

            <p>
                <input type="submit" value="Guardar" class="btn btn-success">
            </p>

            <a href="/CineKinal2009190/public/Cartelera" class="btn btn-default">Regresar</a>
        </form>
    </div>
</div>
</div>

@if(Session::has('message'))
    <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
@endif
</body>
</html>