<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::resource('Peliculas','PeliculasController');
Route::resource('Cines','CinesController');
Route::resource('Salas','SalasController');
Route::resource('Tiposala','TipoSalasController');
Route::resource('FormatoPelicula','FormatoPeliculasController');
Route::resource('Carteleras','CartelerasController');


Route::post('/Pelicula/store','VistaPeliculas@store');
Route::post('/Pelicula/update/{id}','VistaPeliculas@update');
Route::get('/Pelicula/destroy/{id}','VistaPeliculas@destroy');


Route::post('/Cine/update/{id}','VistaCine@update');
Route::get('/Cine/destroy/{id}','VistaCine@destroy');
Route::post('/Cine/store','VistaCine@store');

Route::post('/Cartelera/update/{id}','VistaCartelera@update');
Route::get('/Cartelera/destroy/{id}','VistaCartelera@destroy');
Route::post('/Cartelera/store','VistaCartelera@store');

Route::controller('Pelicula','VistaPeliculas');
Route::controller('Cine','VistaCine');
Route::controller('Sala', 'VistaSala');
Route::controller('Tipof', 'VistaTipo');
Route::controller('Cartelera', 'VistaCartelera');

Route::resource('Prueba','PeliculasController@CarteleraCine');